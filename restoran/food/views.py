from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import DestroyAPIView, UpdateAPIView, RetrieveAPIView
from accounts.permissions import GroupBasePermission, IsOwnerOrReadOnly
from restoran.decorators import can_create_food, can_update_product, can_delete_product
from .serializers import (
    FoodSerializer,
    TableSerializer,
    DepartmentSerializer,
    FoodCategorySerializer

)
from .models import (
    Food,
    Table,
    Department,
    FoodCategory
)
from rest_framework import generics, permissions


class FoodViewList(generics.ListAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer


# class FoodViewCreate(APIView):
#
#     def post(self, request, format=None):
#         serializer = FoodSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FoodCreateAPIView(generics.CreateAPIView):
    serializer_class = FoodSerializer
    permission_classes = [permissions.IsAuthenticated, GroupBasePermission]


class FoodViewDelete(DestroyAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer


class FoodViewUpdate(UpdateAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer


class FoodViewDetail(RetrieveAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer
    lookup_field = 'id'


class TableViewList(generics.ListCreateAPIView):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class TableViewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class DepartmentViewList(generics.ListCreateAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class DepartmentViewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class FoodCategoryViewList(generics.ListCreateAPIView):
    queryset = FoodCategory.objects.all()
    serializer_class = FoodCategorySerializer


class FoodCategoryViewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FoodCategory.objects.all()
    serializer_class = FoodCategorySerializer








