from django.contrib import admin
from .models import (
        Table,
        FoodCategory,
        Food,
        Department,
)

admin.site.register(Table)
# admin.site.register(Role)
admin.site.register(FoodCategory)
admin.site.register(Food)
admin.site.register(Department)
# admin.site.register(ServicePercentage)
# admin.site.register(Order)
# admin.site.register(Check)
# admin.site.register(Status)

