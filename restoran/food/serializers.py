from rest_framework import serializers
from rest_framework.reverse import reverse as api_reverse
from .models import (
    Food,
    Table,
    Department,
    FoodCategory,
)


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = (
            'id',
            'name_of_tables',
            'is_free',
        )


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = (
            'id',
            'name_of_departments',
        )


class FoodCategorySerializer(serializers.ModelSerializer):
    department = serializers.PrimaryKeyRelatedField(
        queryset=Department.objects.all()
    )

    class Meta:
        model = FoodCategory
        fields = (
            'id',
            'title',
            'department',
        )


class FoodSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)
    category = serializers.PrimaryKeyRelatedField(
        queryset=FoodCategory.objects.all()
    )
    category_name = FoodCategorySerializer(
        source="category",
        read_only=True,
    )

    class Meta:
        model = Food
        fields = (
            'id',
            'title',
            'price',
            'description',
            'category_name',
            'category',
            'uri'
        )

    def get_uri(self, obj):
        request = self.context.get('request')
        return api_reverse("api-food:food_details", kwargs={"id": obj.id}, request=request)


# class OrderSerializer(serializers.ModelSerializer):
#     table = serializers.PrimaryKeyRelatedField(
#         queryset=Table.objects.all()
#     )
#     table_name = TableSerializer(
#         source='table',
#         read_only=True,
#     )
#     food = serializers.SlugRelatedField(
#         queryset=Food.objects.all(),
#         many=True,
#         slug_field='name_of_food',
#
#     )
#     meals_name = FoodSerializer(
#          source='meal',
#          read_only=True,
#          many=True,
#     )
#
#     class Meta:
#         model = Order
#         fields = (
#             'id',
#             'isitopen',
#             'date',
#             'table',
#             'table_name',
#             'meal',
#             'meals_name',
#         )
